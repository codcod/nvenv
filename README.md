# nvenv

Simple script with helper functions to easily create separate Neovim configuration directories (a.k.a. virtual environments) and switch between them in order to subsequently test different Neovim setups. At least that's the idea.

Download with:

    git clone https://codeberg.org/codcod/nvenv.git ~/.local/bin/nvenv

Add to `$PATH` in ZSH (`.zshrc`):

    path+=("$HOME/.local/bin/nvenv/")

Usage:

    nvenv -h

Basic use case:

    nvenv init
    nvenv create astrovim
    nvenv ls
    nvenv pick astrovim

    git clone https://github.com/kabinspace/AstroVim ~/.config/nvim
    nvim +PackerSync

    nvenv pick default
    nvenv rm astrovim
    nvenv uninstall

What it does? Line by line:

1. create necessary symlinks and move present configuration to the 'default' environment
1. list available environments
1. activate the new (empty) environment that will acommodate the AstroVim setup
1. install AstroVim
1. configure AstroVim
1. go back to the original setup
1. remove the new environment, delete the whole AstroVim setup
1. restore original configuration from before, make 'default' environment the only one


## Addenda

### Bash on MacOS

Originally MacOS comes with GNU Bash 3.2 from 2007. To update it across the whole system, simply run `brew install bash` and then add the new one to `/etc/shells`. See the last entry below:

    $ cat /etc/shells          
    # List of acceptable shells for chpass(1).
    # Ftpd will not allow users to connect who are not using
    # one of these shells.

    /bin/bash
    /bin/csh
    /bin/dash
    /bin/ksh
    /bin/sh
    /bin/tcsh
    /bin/zsh
    /usr/local/bin/bash

